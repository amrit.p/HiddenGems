package nirvaniclabs.com.hiddengems.fragments;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.adapters.CategoryAdapter;
import nirvaniclabs.com.hiddengems.broadcastreceiversandservices.TripProgressEvents;
import nirvaniclabs.com.hiddengems.customwidgets.CustomTextView;
import nirvaniclabs.com.hiddengems.models.Flower;
import nirvaniclabs.com.hiddengems.network.HiddenGemsService;
import nirvaniclabs.com.hiddengems.network.HttpConnection;
import nirvaniclabs.com.hiddengems.utils.Constants;
import nirvaniclabs.com.hiddengems.utils.HiddenGemsApplication;
import nirvaniclabs.com.hiddengems.utils.Utility;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This fragment is used to display on-going trip. User can pause trip, go to his current location,
 * and access info about trip and completed POI's from here. User can filter the POI based on the poi filters.
 * In Free roam mode all the floating action buttons will be invisible.
 * In Explore mode trip info floating icon is invisible.
 */
public class TripFragment extends Fragment implements LocationListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, Callback<List<Flower>>, GpsStatus.Listener, Utility.NetworkCheckerInterFace {

    private AppCompatActivity mActivity;
    private HiddenGemsApplication mHiddenGemsApplication;

    public static final String TAG = "TripFragment";
    private static String mActionBarTitle;
    private static final String TRANSLATION_Y = "translationY";
    private boolean expanded = false, mIsUserPassedSourcePoint, mIsInternetConnected, mIsTripCompleted;
    private float offset1, offset2, offset3, mTotalTripDistance;
    private ArrayList<Flower> mFlowerList;

    private MapView mMapView;
    private GoogleMap mMap;
    private RelativeLayout mFabContainer;
    private RecyclerView mRecyclerView;
    private CustomTextView mEmptyView;
    private FloatingActionButton mActionButtonCurrentLocation, mActionButtonPause, mActionButtonTripInfo, mActionButtonEnableDisableMystryLocation, mActionButtonMenu;

    // This variable is used to save map positions during life cycle of this fragment
    private CameraPosition mCameraPosition;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private Marker mMarkerCurrentLocation, mMarkerDestination;
    private Polyline mPolyline;
    private LocationManager mLocationManager;
    private float bearingTo;

    public TripFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (mActivity != null)
            mHiddenGemsApplication = (HiddenGemsApplication) mActivity.getApplication();
        else {
            mActivity = (AppCompatActivity) getActivity();
            mHiddenGemsApplication = (HiddenGemsApplication) mActivity.getApplication();
        }

        super.onCreate(savedInstanceState);
        try {
            ActionBar actionBar = mActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            setHasOptionsMenu(true);

            mLocationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);

            createLocationRequestAndInitGoogleApiClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {

            if (mActivity == null)
                mActivity = (AppCompatActivity) getActivity();

            if (Utility.get_instance(mActivity).googleServiceIsOK(mActivity)) {
                ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_trip, container, false);

                if (getArguments() != null)
                    mActionBarTitle = getArguments().getString(Constants.ARGUMENT_ACTIONBAR_TITLE);

                initViews(viewGroup);

                if (mMapView != null)
                    mMapView.onCreate(savedInstanceState);

                checkIfMapLoaded();

                setUpFloatingActionIcons();

                if (initMap()) {
                    // Shivaji Nagar is the center of the map
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.CITY_POST_PUNE_LAT, Constants.CITY_POST_PUNE_LNG), 14.0f);
                    mMap.moveCamera(cameraUpdate);

                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            return true;
                        }
                    });

                    mActionButtonCurrentLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mCurrentLocation != null && mMarkerCurrentLocation != null) {
                                mMarkerCurrentLocation.remove();

                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation));
                                mMarkerCurrentLocation = mMap.addMarker(markerOptions);
                                mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude())));

                            }
                        }
                    });

                    if (HomeActivity.mLatLngSource != null) {  // This will add source Marker on map
                        MarkerOptions sourceMarkerOptions = new MarkerOptions();
                        sourceMarkerOptions.position(HomeActivity.mLatLngSource);
                        mMap.addMarker(sourceMarkerOptions);
                    }

                    if (HomeActivity.mLatLngDestination != null) {
                        MarkerOptions destinationMarkerOptions = new MarkerOptions();
                        destinationMarkerOptions.position(HomeActivity.mLatLngDestination);
                        mMarkerDestination = mMap.addMarker(destinationMarkerOptions);

                        // destination marker is invisible until user reaches the source point
                        mMarkerDestination.setVisible(false);

                        // This will be total trip distance
                        mTotalTripDistance = Utility.get_instance(mActivity).distanceBetween(HomeActivity.mLatLngSource, HomeActivity.mLatLngDestination);

                        Location location = new Location("A");
                        location.setLatitude(HomeActivity.mLatLngSource.latitude);
                        location.setLongitude(HomeActivity.mLatLngSource.longitude);

                        Location locationB = new Location("B");
                        locationB.setLatitude(HomeActivity.mLatLngDestination.latitude);
                        locationB.setLongitude(HomeActivity.mLatLngDestination.longitude);

//                        bearingTo = SphericalUtil.computeHeading(HomeActivity.mLatLngSource,HomeActivity.mLatLngDestination);
                        bearingTo = location.bearingTo(locationB);
                    }

                    // This function will retrieve data from server on background thread
                    getDataFromServer();

                } else
                    Toast.makeText(mActivity, "Map is not available.", Toast.LENGTH_LONG).show();

                return viewGroup;
            } else
                return inflater.inflate(R.layout.no_google_play_service_message, container, false);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

        if (!Constants.mIsFreeRoamMode) {
            EventBus.getDefault().post(new TripProgressEvents(0, mActionBarTitle));
            ((HomeActivity) mActivity).setTripPogressLayoutVisibility(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) mActivity).setDrawerIndicatorEnabled(true);
        ((HomeActivity) mActivity).setActionBarTitle(mActionBarTitle);
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_UNLOCKED);

        if (mMapView != null)
            mMapView.onResume();

        initMap();

        if (mCameraPosition != null) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
            mCameraPosition = null;
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && !mIsTripCompleted)
            startLocationUpdates();

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (mLocationManager != null && !mIsTripCompleted)
            mLocationManager.addGpsStatusListener(this);

        if (!mIsTripCompleted)
            Utility.get_instance(mActivity).isInternetConnected(TripFragment.this);

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null)
            mMapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null)
            mMapView.onPause();

        stopLocationUpdates();
        mLocationManager.removeGpsStatusListener(this);
        mCameraPosition = mMap.getCameraPosition();
        mMap = null;

        // This will remove all the callbacks present on mHandler
//        mHandler.removeCallbacks(mRunnableRouteDrawer);
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView called");
        mActivity = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (mMapView != null)
            mMapView.onDestroy();
        mActionBarTitle = null;
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null)
            mMapView.onSaveInstanceState(outState);
    }

    private void createLocationRequestAndInitGoogleApiClient() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(Constants.LOCATION_UPDATE_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        displayGPSEnableDialog();
    }

    /**
     * Displays dialog to enable system GPS
     */
    private void displayGPSEnableDialog() {

        if (mLocationRequest == null || mGoogleApiClient == null)
            return;

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(mActivity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    private void getDataFromServer() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://services.hanselandpetal.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HiddenGemsService hiddenGemsService = retrofit.create(HiddenGemsService.class);

        Call<List<Flower>> data = hiddenGemsService.getData();

        // asynchronous call
        data.enqueue(this);
    }

    private void setUpFloatingActionIcons() {
        mActionButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expanded = !expanded;
                if (expanded) {
                    expandFab();
                } else {
                    collapseFab();
                }

            }
        });

        mFabContainer.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        mFabContainer.getViewTreeObserver().removeOnPreDrawListener(this);
                        offset1 = mActionButtonMenu.getY() - mActionButtonPause.getY() + 20;
                        mActionButtonPause.setTranslationY(offset1);
                        offset2 = mActionButtonMenu.getY() - mActionButtonTripInfo.getY() + 20;
                        mActionButtonTripInfo.setTranslationY(offset2);
                        offset3 = mActionButtonMenu.getY() - mActionButtonEnableDisableMystryLocation.getY() + 20;
                        mActionButtonEnableDisableMystryLocation.setTranslationY(offset3);
                        return true;
                    }
                }
        );
    }

    private void initViews(ViewGroup viewGroup) {
        mMapView = (MapView) viewGroup.findViewById(R.id.mapview);
        mFabContainer = (RelativeLayout) viewGroup.findViewById(R.id.relative_lay_fab_container);
        mActionButtonMenu = (FloatingActionButton) viewGroup.findViewById(R.id.floating_button_menu);
        mActionButtonPause = (FloatingActionButton) viewGroup.findViewById(R.id.floating_button_pause);
        mActionButtonTripInfo = (FloatingActionButton) viewGroup.findViewById(R.id.floating_button_trip_info);
        mActionButtonEnableDisableMystryLocation = (FloatingActionButton) viewGroup.findViewById(R.id.floating_button_mystry_locations);
        mActionButtonCurrentLocation = (FloatingActionButton) viewGroup.findViewById(R.id.floating_button_current_location);
        mRecyclerView = (RecyclerView) viewGroup.findViewById(R.id.recycler_view_horizontal);
        mEmptyView = (CustomTextView) viewGroup.findViewById(R.id.text_empty_view);

        if (Constants.mIsExploreMode) {
            mActionButtonTripInfo.setVisibility(View.GONE);
        }

        // do not use setHasFixedSize when layout manager is staggered
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient != null && mLocationRequest != null)
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_result, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                ((HomeActivity) mActivity).handleFrames(true);
                FilterPoiFragment filterPoiFragment = new FilterPoiFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.ARGUMENT_ACTIONBAR_TITLE, mActionBarTitle);
                filterPoiFragment.setArguments(bundle);
                setMenuVisibility(false);
                ((HomeActivity) mActivity).addNewFragment(filterPoiFragment, mActivity.getString(R.string.filter), FilterPoiFragment.TAG);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void expandFab() {

        mActionButtonMenu.setImageResource(R.drawable.ic_plusone_medium_off_client);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(createExpandAnimator(mActionButtonPause, offset1),
                createExpandAnimator(mActionButtonTripInfo, offset2),
                createExpandAnimator(mActionButtonEnableDisableMystryLocation, offset3));
        animatorSet.start();
        animateFab();
    }

    private void collapseFab() {
        mActionButtonMenu.setImageResource(R.drawable.common_full_open_on_phone);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(createCollapseAnimator(mActionButtonPause, offset1),
                createCollapseAnimator(mActionButtonTripInfo, offset2),
                createCollapseAnimator(mActionButtonEnableDisableMystryLocation, offset3));
        animatorSet.start();
        animateFab();
    }

    private Animator createCollapseAnimator(View view, float offset) {
        return ObjectAnimator.ofFloat(view, TRANSLATION_Y, 0, offset)
                .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    private Animator createExpandAnimator(View view, float offset) {
        return ObjectAnimator.ofFloat(view, TRANSLATION_Y, offset, 0)
                .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    private void animateFab() {
        Drawable drawable = mActionButtonMenu.getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
    }


    /**
     * This will display alert if map is unable to load due to internet connectivity.
     */
    private void displayAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setMessage(message)
                .setTitle(getResources().getString(R.string.app_name));

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();

        dialog.setCancelable(false);

        dialog.show();
    }

    /**
     * This method checks if map is available or not
     *
     * @return true if map available else false
     */
    private boolean initMap() {

        if (mMapView == null)
            return false;

        if (mMap == null) {
            mMap = mMapView.getMap();
        }
        return (mMap != null);
    }

    private String getMapDirectionsAPIUrl(LatLng source, LatLng target) {

        if (source == null || target == null)
            return null;
        String Url = "https://maps.googleapis.com/maps/api/directions/json";

        String origin = "origin=" + Double.toString(source.latitude) + "," + Double.toString(source.longitude);

        String destination = "&destination=" + Double.toString(target.latitude) + "," + Double.toString(target.longitude);

//        String WayPoints = "&waypoints=optimize:true|" + Double.toString(Constants.CITY_POST_PUNE_LAT) + "," + Double.toString(Constants.CITY_POST_PUNE_LNG);

//        String params = origin + destination + WayPoints + "&sensor=false&mode=driving&alternatives=true&region=in&key=AIzaSyA5swFaHUligp59tG_3mJufW9_dK3_byhI";

        String params = origin + destination + "&sensor=false&mode=driving&alternatives=true&region=in&key=AIzaSyA5swFaHUligp59tG_3mJufW9_dK3_byhI";

        Url = Url + "?" + params;

        return Url;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected Called");

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && !mIsTripCompleted)
            startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended called");
    }

    private Runnable mRunnableInterNetChecker = new Runnable() {
        @Override
        public void run() {

            Utility.get_instance(mActivity).isInternetConnected(TripFragment.this);
        }
    };

    private Runnable mRunnableRouteDrawer = new Runnable() {
        @Override
        public void run() {
            drawRouteBetweenPoints();
        }
    };

    private Handler mHandler = new Handler();

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged Called");

        if (mIsTripCompleted)
            return;

        mCurrentLocation = location;

        long delay = 0;
        if (mPolyline != null)
            delay = Constants.DEFAULT_DELAY_BETWEEN_CALLS;

        mHandler.postDelayed(mRunnableRouteDrawer, delay);

        if (mMap != null && mCurrentLocation != null) {

            if (mMarkerCurrentLocation == null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation));
                mMarkerCurrentLocation = mMap.addMarker(markerOptions);

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()))             // Sets the center of the map to current location
                        .zoom(15)                   // Sets the zoom
                        .bearing(bearingTo) // Sets the orientation of the camera to the route direction
                        .tilt(0)                   // Sets the tilt of the camera to 0 degrees
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else {
                mMarkerCurrentLocation.remove();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation));
                markerOptions.position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                mMarkerCurrentLocation = mMap.addMarker(markerOptions);

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()))             // Sets the center of the map to current location
                        .zoom(mMap.getCameraPosition().zoom)                   // Sets the zoom
                        .bearing(bearingTo) // Sets the orientation of the camera to east
                        .tilt(0)                   // Sets the tilt of the camera to 0 degrees
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }

    }


    /**
     * @return completed distance of a trip
     */
    float calculateTripProgress() {
        float completedDistance = Utility.get_instance(mActivity).distanceBetween(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), HomeActivity.mLatLngSource);

        Toast.makeText(mActivity, "completedDistance " + completedDistance + " \n mTotalTripDistance  " + mTotalTripDistance, Toast.LENGTH_LONG).show();

        // if gap between current location and destination is of 50 meters, trip is completed
        if ((mTotalTripDistance - completedDistance) <= Constants.DEFAULT_ERROR_FOR_TRIP) {
            return 100;
        } else {
            float v = completedDistance * 100;
            return (v / mTotalTripDistance);
        }
    }

    private void drawRouteBetweenPoints() {

        //  check if source point and Current location is at same place
        if (!checkUserAtSource()) {

            if (Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                String mapDirectionsAPIUrl = getMapDirectionsAPIUrl(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), HomeActivity.mLatLngSource);
                if (!TextUtils.isEmpty(mapDirectionsAPIUrl))
                    new DirectionAPITask().execute(mapDirectionsAPIUrl);
            }
        } else {

            // TODO: start the trip,draw route between source and destination, also start trip progress updates, drawing of completed route,check for trip completion

            if (Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {

                if (mMarkerDestination != null) mMarkerDestination.setVisible(true);
                else {
                    mEmptyView.setText("You've reached the source point.");
                    return;
                }

                mEmptyView.setVisibility(View.GONE);
                mIsUserPassedSourcePoint = true;
                mRecyclerView.setVisibility(View.VISIBLE);

                String mapDirectionsAPIUrl = getMapDirectionsAPIUrl(HomeActivity.mLatLngSource, HomeActivity.mLatLngDestination);
                if (!TextUtils.isEmpty(mapDirectionsAPIUrl))
                    new DirectionAPITask().execute(mapDirectionsAPIUrl);

                float tripProgress = calculateTripProgress();

                BigDecimal bd = new BigDecimal(Float.toString(tripProgress));
                bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
                float roundOffDistance = bd.floatValue();

                EventBus.getDefault().post(new TripProgressEvents(roundOffDistance, mActionBarTitle));

                if (tripProgress == 100) {
                    mIsTripCompleted = true;
                    completeTheTrip();
                } else {
                    // This will tell HomeActivity about tripCompletion
                    EventBus.getDefault().post(false);
                    mIsTripCompleted = false;
                }
            }

        }
    }

    // TODO: check the comment below and implement method accordingly.

    /**
     * This API is called on trip completion.
     * Here we are displaying trip completion alert, stopping location updates,changes trip status from pause to completed.
     * If this trip was resumed then mark it as completed on server.
     * Remove all the displayed routes and markers
     * Remove all the callbacks from handler.
     * Stop for listening gps status listener.
     * Disable trip pause button.
     * Disable option to enable all mystery points.
     * Also set flag to remove trip pause alert in HomeActivity.
     */
    private void completeTheTrip() {

        stopLocationUpdates();

        mActionButtonPause.setEnabled(false);
        mActionButtonEnableDisableMystryLocation.setEnabled(false);
        mActionButtonCurrentLocation.setEnabled(false);

        mHandler.removeCallbacksAndMessages(null);

        mLocationManager.removeGpsStatusListener(this);

        if (mPolyline != null)
            mPolyline.remove();

        if (mMap != null)
            mMap.clear();

        EventBus.getDefault().post(true);

        // display trip completion alert
        displayAlert("Congrats\nYou've successfully completed the trip.");
    }

    /**
     * @return true if user is reached the source point
     */
    private boolean checkUserAtSource() {

        if (mIsUserPassedSourcePoint)
            return true;

        float distanceInKiloMeters = Utility.get_instance(mActivity).distanceBetween(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), HomeActivity.mLatLngSource);

        float distanceInMeters = distanceInKiloMeters * 1000;

//        BigDecimal bd = new BigDecimal(Float.toString(distanceInKiloMeters));
//        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
//        float roundOffDistance = bd.floatValue();

        if (distanceInMeters > Constants.DEFAULT_DISTANCE_BETWEEN_CURRENT_LOCATION_AND_SOURCE) {
            mIsUserPassedSourcePoint = false;
            if (distanceInKiloMeters < 1.0)
                mEmptyView.setText("Source is " + (distanceInKiloMeters * 1000) + " m away.");
            else
                mEmptyView.setText("Source is " + distanceInKiloMeters + " Km away.");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed called");
    }

    @Override
    public void onResponse(Response<List<Flower>> response, Retrofit retrofit) {

        if (response.body() != null && response.isSuccess()) {

            if (mFlowerList == null)
                mFlowerList = new ArrayList<>();

            mFlowerList.addAll(response.body());

            if (mFlowerList.size() >= 0) {

                // initiate adapter and display data
                CategoryAdapter categoryAdapter = new CategoryAdapter(mFlowerList, mActivity);
                mRecyclerView.setAdapter(categoryAdapter);
            }
        }
    }

    @Override
    public void onFailure(Throwable t) {

    }

    @Override
    public void onGpsStatusChanged(int event) {

        switch (event) {
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                break;
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                break;
            case GpsStatus.GPS_EVENT_STARTED:
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                displayGPSEnableDialog();
                break;
        }
    }

    @Override
    public void setIsNetworkAvailable(boolean isNetworkAvailable) {
        mIsInternetConnected = isNetworkAvailable;
        mHandler.postDelayed(mRunnableInterNetChecker, Constants.DEFAULT_DELAY_BETWEEN_CALLS);
    }

    private class DirectionAPITask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String data = "";
            HttpConnection httpConnection = new HttpConnection();
            try {
                if (mIsInternetConnected)
                    data = httpConnection.readUrl(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            drawPath(s);
        }
    }

    /**
     * This draws path between source and destination
     */
    public void drawPath(String result) {

        try {
            //Tranform the string into a json object
            if (TextUtils.isEmpty(result))
                return;

            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
//            List<LatLng> list = decodePoly(encodedString);
            List<LatLng> list = PolyUtil.decode(encodedString);
            if (list != null && list.size() > 0) {
                if (mMap != null) {
                    if (mPolyline != null)
                        mPolyline.remove();
                    mPolyline = mMap.addPolyline(new PolylineOptions()
                                    .addAll(list)
                                    .width(6)
                                    .color(Color.parseColor("#05b1fb"))//Google maps blue color
                                    .geodesic(true)
                    );
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private List<LatLng> decodePoly(String encoded) {
//
//        List<LatLng> poly = new ArrayList<>();
//        int index = 0, len = encoded.length();
//        int lat = 0, lng = 0;
//
//        while (index < len) {
//            int b, shift = 0, result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lat += dlat;
//
//            shift = 0;
//            result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lng += dlng;
//
//            LatLng p = new LatLng((((double) lat / 1E5)),
//                    (((double) lng / 1E5)));
//            poly.add(p);
//        }
//
//        return poly;
//    }

    private void checkIfMapLoaded() {
        try {

            if (mHiddenGemsApplication.mGlobalSharedPreferences.getBoolean(Constants.PREFS_IS_MAP_LOADED_ON_TRIP_SCREEN, false))
                return;

            final int DOESNT_EXIST = -1;

            int currentVersionCode;
            try {
                currentVersionCode = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionCode;
            } catch (android.content.pm.PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return;
            }

            int savedVersionCode = mHiddenGemsApplication.mGlobalSharedPreferences.getInt(Constants.PREF_VERSION_CODE_KEY, DOESNT_EXIST);


            // Check for first run or upgrade
            if (currentVersionCode == savedVersionCode) {

                // This is just a normal run
                if (!mHiddenGemsApplication.mGlobalSharedPreferences.getBoolean(Constants.PREFS_IS_MAP_LOADED_ON_TRIP_SCREEN, false) && (!Utility.get_instance(mActivity).isNetworkConnected(mActivity))) {
                    displayAlert(mActivity.getString(R.string.no_internet_while_mapload));
                } else
                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_TRIP_SCREEN, true).apply();
                return;

            } else if (savedVersionCode == DOESNT_EXIST) {

                // This is a new install (or the user cleared the shared preferences)

                // Check for network connection and if no network then display alert

                if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {

                    displayAlert(mActivity.getString(R.string.no_internet_while_mapload));

                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_TRIP_SCREEN, false).apply();

                } else
                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_TRIP_SCREEN, true).apply();
            }

        /*else if (currentVersionCode > savedVersionCode) {

              This is an upgrade

        }*/

            // Update the shared preferences with the current version code
            mHiddenGemsApplication.mGlobalSharedPreferences.edit().putInt(Constants.PREF_VERSION_CODE_KEY, currentVersionCode).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}