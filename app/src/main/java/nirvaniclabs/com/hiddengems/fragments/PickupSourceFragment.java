package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.adapters.PlaceAutocompleteAdapter;
import nirvaniclabs.com.hiddengems.customwidgets.CustomAutoCompleteTextView;
import nirvaniclabs.com.hiddengems.customwidgets.CustomButton;
import nirvaniclabs.com.hiddengems.utils.Constants;
import nirvaniclabs.com.hiddengems.utils.HiddenGemsApplication;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * /**
 * This fragment is used to accept source of a trip from user.
 * Source can be taken from Map or through autocomplete view or it'll be user's current location.
 * This will be used in explore mode
 */
public class PickupSourceFragment extends Fragment implements OnConnectionFailedListener, LocationListener, GoogleApiClient.ConnectionCallbacks {

    private AppCompatActivity mActivity;
    public static String TAG = "PickupSourceFragment";
    private HiddenGemsApplication mHiddenGemsApplication;
    private GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;

    private MapView mMapView;
    private CustomButton mButtonNext;
    private ImageButton mImageButtonNextArrow, mImageButtonPickUpCurrentLocation;
    private GoogleMap mMap;
    AlertDialog mAlertDialog;
    private CustomAutoCompleteTextView mCustomAutoCompleteTextView;
    private LatLng mLatlngAutocompleLocation, mLatLngSource;
    private String mSourceName;

    // This variable is used to save map positions during life cycle of this fragment
    private CameraPosition mCameraPosition;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private Marker mMarkerAutoCompleteAPI, mMarkerCurrentLocation;

    public PickupSourceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (mActivity != null)
            mHiddenGemsApplication = (HiddenGemsApplication) mActivity.getApplication();
        else {
            mActivity = (AppCompatActivity) getActivity();
            mHiddenGemsApplication = (HiddenGemsApplication) mActivity.getApplication();
        }

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        try {
            Utility.adjustSoftInput(mActivity);
            ActionBar actionBar = mActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
            }

            createLocationRequestAndInitGoogleAPIClient();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {

            if (mActivity == null)
                mActivity = (AppCompatActivity) getActivity();

            if (Utility.get_instance(mActivity).googleServiceIsOK(mActivity)) {

                ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_pickup_start_location, container, false);

                initViews(viewGroup);

                if (mMapView != null)
                    mMapView.onCreate(savedInstanceState);

                checkIfMapLoaded();

                mImageButtonNextArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (TextUtils.isEmpty(mCustomAutoCompleteTextView.getText().toString())) {
                            Toast.makeText(mActivity, R.string.source_place_invalid, Toast.LENGTH_LONG).show();
                            return;
                        }

                        goForDestination(mCustomAutoCompleteTextView.getText().toString());
                    }
                });

                mButtonNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (TextUtils.isEmpty(mCustomAutoCompleteTextView.getText().toString())) {
                            Toast.makeText(mActivity, R.string.source_place_invalid, Toast.LENGTH_LONG).show();
                            return;
                        }

                        goForDestination(mCustomAutoCompleteTextView.getText().toString());
                    }
                });

                if (initMap()) {

                    // Shivaji Nagar is the center of the map
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.CITY_POST_PUNE_LAT, Constants.CITY_POST_PUNE_LNG), 14.0f);
                    mMap.moveCamera(cameraUpdate);

                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            return true;
                        }
                    });

                    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {

                            if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                                Toast.makeText(mActivity, mActivity.getString(R.string.no_internet_message), Toast.LENGTH_LONG).show();
                                return;
                            }


                            String place = convertLatLngToAddress(latLng);

                            if (!TextUtils.isEmpty(place)) {

                                if (mMarkerAutoCompleteAPI != null) {
                                    mMarkerAutoCompleteAPI.remove();
                                }

                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                mMarkerAutoCompleteAPI = mMap.addMarker(markerOptions);
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                                mCustomAutoCompleteTextView.setText(place);
                                mLatLngSource = latLng;
                                mLatlngAutocompleLocation = latLng;
                                mSourceName = place;
                            } else {
                                Toast.makeText(mActivity, R.string.error_convert_to_physical_address, Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    // Register a listener that receives callbacks when a suggestion has been selected
                    mCustomAutoCompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
                    // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
                    // the entire world.
                    mAdapter = new PlaceAutocompleteAdapter(mActivity, mGoogleApiClient, Constants.BOUNDS_PUNE_CITY,
                            null);
                    mCustomAutoCompleteTextView.setAdapter(mAdapter);

                    mImageButtonPickUpCurrentLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mCurrentLocation != null && mMarkerCurrentLocation != null) {

                                if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
                                    Toast.makeText(mActivity, mActivity.getString(R.string.no_internet_message), Toast.LENGTH_LONG).show();
                                    return;
                                }

                                mMarkerCurrentLocation.remove();

                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation));
                                mMarkerCurrentLocation = mMap.addMarker(markerOptions);
                                mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude())));

                                String currentLocation = convertLatLngToAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));

                                if (!TextUtils.isEmpty(currentLocation)) {

                                    mCustomAutoCompleteTextView.setText(currentLocation);
                                    mLatLngSource = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

                                } else {
                                    Toast.makeText(mActivity, R.string.error_convert_to_physical_address, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(mActivity, R.string.no_location_found, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                } else
                    Toast.makeText(mActivity, "Map is not available.", Toast.LENGTH_LONG).show();

                return viewGroup;
            } else {
                return inflater.inflate(R.layout.no_google_play_service_message, container, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    /**
     * Creates Location request object,To store parameters for requests to the fused location provider
     * AND
     * Initiates google api client
     * ALSO
     * Check if user GPS is enabled or not
     * IF NOT
     * Asks user to enable it
     */
    private void createLocationRequestAndInitGoogleAPIClient() {
        mLocationRequest = new LocationRequest();
//        rate in milliseconds at which your app prefers to receive location updates
        mLocationRequest.setInterval(Constants.LOCATION_UPDATE_INTERVAL);
//        fastest rate in milliseconds at which your app can handle location updates.
        mLocationRequest.setFastestInterval(Constants.LOCATION_UPDATE_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();

        // automatically enables GPS
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(mActivity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    /**
     * This redirects us to pick destination point if user has provided the start location.
     */
    private void goForDestination(String place) {

        // check if  user has selected his current location or selected location from autocomplet view

        if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {
            Toast.makeText(mActivity, mActivity.getString(R.string.no_internet_message), Toast.LENGTH_LONG).show();
            return;
        }

        if (!checkLocationIsInCityBoundary(mLatLngSource)) {
            Toast.makeText(mActivity, R.string.vaild_city_boundry, Toast.LENGTH_LONG).show();
            return;
        }

        displaySourceConfirmationDialog(place);
    }


    /**
     * This API checks if selected location is in the boundry of city or not
     *
     * @return true if distance is in city boundaries
     */
    private boolean checkLocationIsInCityBoundary(LatLng latLng) {
        if (latLng != null) {

            float distanceInKiloMeters = Utility.get_instance(mActivity).distanceBetween(new LatLng(Constants.CITY_POST_PUNE_LAT, Constants.CITY_POST_PUNE_LNG), latLng);
            if (distanceInKiloMeters > Constants.DEFAULT_RADIUS_PUNE)
                return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((HomeActivity) mActivity).setDrawerIndicatorEnabled(false);
            ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            ((HomeActivity) mActivity).setActionBarTitle(mActivity.getString(R.string.explore));

            if (mMapView != null)
                mMapView.onResume();

            initMap();

            if (mCameraPosition != null) {
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
                mCameraPosition = null;
            }

            if (mLatlngAutocompleLocation != null && !TextUtils.isEmpty(mSourceName)) {
                mMarkerAutoCompleteAPI = mMap.addMarker(new MarkerOptions().title(mSourceName).position(mLatlngAutocompleLocation));
                mMap.animateCamera(CameraUpdateFactory.newLatLng(mLatlngAutocompleLocation));
            }

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
                startLocationUpdates();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient != null && mLocationRequest != null)
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the Place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the Place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Utility.hideKeypadForAutomCompleteView(mCustomAutoCompleteTextView, mActivity);

            /*
             Retrieve the Place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the Place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            if (!TextUtils.isEmpty(primaryText))
                Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the Place.
              */

            if (!TextUtils.isEmpty(placeId)) {
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                if (placeResult != null)
                    placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

                Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
            }
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first Place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {

            if (places == null)
                return;

            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

//            mMap.clear();
            mLatlngAutocompleLocation = place.getLatLng();
            mLatLngSource = place.getLatLng();
            mSourceName = place.getName().toString();


            if (mMarkerAutoCompleteAPI != null)
                mMarkerAutoCompleteAPI.remove();

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(place.getLatLng());
            markerOptions.title(place.getName().toString());
            mMarkerAutoCompleteAPI = mMap.addMarker(markerOptions);
            mMap.animateCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));

            places.release();
        }
    };


    /**
     * @param latLng whose physical address is required
     *               Converts geographical location to an address
     */
    private String convertLatLngToAddress(LatLng latLng) {

        try {

            Geocoder geocoder = new Geocoder(mActivity);
            // This method should not be called from main thread
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String sublocality = addresses.get(0).getAddressLine(1);
                String city = addresses.get(0).getAddressLine(2);

                String place = "";

                if (!TextUtils.isEmpty(address))
                    place += address;

                if (!TextUtils.isEmpty(sublocality))
                    place = place + "," + sublocality;

                if (!TextUtils.isEmpty(city))
                    place = place + "," + city;

                if (place.length() > 0 && (place.charAt(0) == ','))
                    place = place.substring(1);

                return place;
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void displaySourceConfirmationDialog(String place) {

        Utility.hideKeypadForAutomCompleteView(mCustomAutoCompleteTextView, mActivity);

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle(mActivity.getString(R.string.app_name))
                .setMessage("Do you want to set: " + place + " as your starting point ?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                if (mAlertDialog != null) {
                    mAlertDialog.dismiss();
                }

                HomeActivity.mLatLngSource = mLatLngSource;
                ((HomeActivity) mActivity).replaceFragment(new PickupDestinationFragment(), mActivity.getString(R.string.explore), PickupDestinationFragment.TAG);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mAlertDialog != null) {
                    mAlertDialog.dismiss();
                }
            }
        });

        mAlertDialog = builder.create();
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();
    }

    /**
     * @param viewGroup from which views are instantiated
     */
    private void initViews(ViewGroup viewGroup) {
        mMapView = (MapView) viewGroup.findViewById(R.id.mapview);
        mButtonNext = (CustomButton) viewGroup.findViewById(R.id.button_next);
        mImageButtonNextArrow = (ImageButton) viewGroup.findViewById(R.id.imagebutton_arrow);
        mCustomAutoCompleteTextView = (CustomAutoCompleteTextView) viewGroup.findViewById(R.id.edittext_enter_source);
        mImageButtonPickUpCurrentLocation = (ImageButton) viewGroup.findViewById(R.id.imagebutton_current_location);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null)
            mMapView.onPause();

        if (mActivity != null)
            Utility.hideKeypadForAutomCompleteView(mCustomAutoCompleteTextView, mActivity);

        stopLocationUpdates();

        if (mMap != null) {
            mCameraPosition = mMap.getCameraPosition();

            mMap = null;
        }

    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_UNLOCKED);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mMapView != null)
            mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        mActivity = null;
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null)
            mMapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null)
            mMapView.onSaveInstanceState(outState);
    }

    private void displayAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setMessage(R.string.no_internet_while_mapload)
                .setTitle(getResources().getString(R.string.app_name));

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    /**
     * This method checks if map is available or not
     *
     * @return true if map available else false
     */
    private boolean initMap() {

        if (mMapView == null)
            return false;

        if (mMap == null) {
            mMap = mMapView.getMap();
        }
        return (mMap != null);
    }


    /**
     * This method checks if map is loaded when user comes to this screen for the first time
     */
    private void checkIfMapLoaded() {
        try {

            if (mHiddenGemsApplication.mGlobalSharedPreferences.getBoolean(Constants.PREFS_IS_MAP_LOADED_ON_Source_SCREEN, false))
                return;

            final int DOESNT_EXIST = -1;

            int currentVersionCode;
            try {
                currentVersionCode = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionCode;
            } catch (android.content.pm.PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return;
            }

            int savedVersionCode = mHiddenGemsApplication.mGlobalSharedPreferences.getInt(Constants.PREF_VERSION_CODE_KEY, DOESNT_EXIST);


            // Check for first run or upgrade
            if (currentVersionCode == savedVersionCode) {

                // This is just a normal run
                if (!mHiddenGemsApplication.mGlobalSharedPreferences.getBoolean(Constants.PREFS_IS_MAP_LOADED_ON_Source_SCREEN, false) && (!Utility.get_instance(mActivity).isNetworkConnected(mActivity))) {
                    displayAlert();
                } else
                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_Source_SCREEN, true).apply();
                return;

            } else if (savedVersionCode == DOESNT_EXIST) {

                // This is a new install (or the user cleared the shared preferences)

                // Check for network connection and if no network then display alert

                if (!Utility.get_instance(mActivity).isNetworkConnected(mActivity)) {

                    displayAlert();

                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_Source_SCREEN, false).apply();

                } else
                    mHiddenGemsApplication.mGlobalSharedPreferences.edit().putBoolean(Constants.PREFS_IS_MAP_LOADED_ON_Source_SCREEN, true).apply();
            }

        /*else if (currentVersionCode > savedVersionCode) {

              This is an upgrade

        }*/

            // Update the shared preferences with the current version code
            mHiddenGemsApplication.mGlobalSharedPreferences.edit().putInt(Constants.PREF_VERSION_CODE_KEY, currentVersionCode).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(PickupSourceFragment.TAG, "onConnectionFailed called");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(PickupSourceFragment.TAG, "onLocationChanged Called");
        mCurrentLocation = location;
        if (mMap != null && mCurrentLocation != null) {

            if (mMarkerCurrentLocation == null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation));
                mMarkerCurrentLocation = mMap.addMarker(markerOptions);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude())));
            } else {
                mMarkerCurrentLocation.remove();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mylocation));
                markerOptions.position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                mMarkerCurrentLocation = mMap.addMarker(markerOptions);
            }

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(PickupSourceFragment.TAG, "onConnected Called");

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(PickupSourceFragment.TAG, "onConnectionSuspended called");
    }
}
