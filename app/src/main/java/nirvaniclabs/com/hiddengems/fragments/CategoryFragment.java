package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.adapters.CategoryAdapter;
import nirvaniclabs.com.hiddengems.customwidgets.CustomTextView;
import nirvaniclabs.com.hiddengems.models.Flower;
import nirvaniclabs.com.hiddengems.network.HiddenGemsService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * This is the HomeScreen of the application. It will contain all trip categories.
 * Initially categories will be parsed inserted into DB and displayed on the list.
 */
public class CategoryFragment extends Fragment implements Callback<List<Flower>> {

    private Button mButtonTemp;
    private AppCompatActivity mActivity;
    public static String TAG = "CategoryFragment";
    private CustomTextView mEmptyTextView;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    private ArrayList<Flower> mFlowerList;

    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            View viewGroup = inflater.inflate(R.layout.fragment_category, container, false);
            initViews(viewGroup);

            // do not use setHasFixedSize when layout manager is staggered
            mRecyclerView.setHasFixedSize(true);

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mActivity, 2);
            mRecyclerView.setLayoutManager(mLayoutManager);

            mEmptyTextView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://services.hanselandpetal.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            HiddenGemsService hiddenGemsService = retrofit.create(HiddenGemsService.class);

            Call<List<Flower>> data = hiddenGemsService.getData();

            // asynchronous call
            data.enqueue(this);

            mButtonTemp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivity) mActivity).replaceFragment(new PickupSourceFragment(), mActivity.getString(R.string.explore), PickupSourceFragment.TAG);
                }
            });

            return viewGroup;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private void initViews(View viewGroup) {
        mButtonTemp = (Button) viewGroup.findViewById(R.id.btn_temp);
        mRecyclerView = (RecyclerView) viewGroup.findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) viewGroup.findViewById(R.id.progress_bar);
        mEmptyTextView = (CustomTextView) viewGroup.findViewById(R.id.empty_view);
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity.mLatLngSource = null;
        HomeActivity.mLatLngDestination = null;

        ((HomeActivity) mActivity).setDrawerIndicatorEnabled(true);
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_UNLOCKED);
        ((HomeActivity) mActivity).setActionBarTitle(getString(R.string.app_name));
    }

    @Override
    public void onResponse(Response<List<Flower>> response, Retrofit retrofit) {

        mProgressBar.setVisibility(View.GONE);

        if (response.body() != null && response.isSuccess()) {

            if (mFlowerList == null)
                mFlowerList = new ArrayList<>();

            mFlowerList.addAll(response.body());

            if (mFlowerList.size() >= 0) {

                // initiate adapter and display data
                CategoryAdapter categoryAdapter = new CategoryAdapter(mFlowerList, mActivity);
                mRecyclerView.setAdapter(categoryAdapter);
                mEmptyTextView.setVisibility(View.GONE);
            } else {
                mEmptyTextView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                mEmptyTextView.setText(R.string.category_no_records);
            }
        } else {
            mEmptyTextView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mEmptyTextView.setText(R.string.category_no_records);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        mProgressBar.setVisibility(View.GONE);
        mEmptyTextView.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);


        if (t instanceof IOException) {
            mEmptyTextView.setText(mActivity.getString(R.string.no_internet_message));
        } else
            mEmptyTextView.setText(R.string.error_catrgory_data_download);
    }
}
