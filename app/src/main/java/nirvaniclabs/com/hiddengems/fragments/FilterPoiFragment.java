package nirvaniclabs.com.hiddengems.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.activities.HomeActivity;
import nirvaniclabs.com.hiddengems.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 * This class will be used to display all the POI FILTER TYPES which will be used to filter POI's of an on-going trip.
 */
public class FilterPoiFragment extends Fragment {

    private AppCompatActivity mActivity;

    public static String TAG = "FilterPoiFragment";
    private String mTripTitle;

    public FilterPoiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AppCompatActivity)
            mActivity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ActionBar actionBar = mActivity.getSupportActionBar();

            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
//            setHasOptionsMenu(false);
//            setMenuVisibility(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_filter_poi, container, false);

            if (getArguments() != null)
                mTripTitle = getArguments().getString(Constants.ARGUMENT_ACTIONBAR_TITLE);

            return viewGroup;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) mActivity).setDrawerIndicatorEnabled(false);
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onStop() {
        ((HomeActivity) mActivity).enableDisableDrawer(DrawerLayout.LOCK_MODE_UNLOCKED);
        ((HomeActivity) mActivity).setDrawerIndicatorEnabled(true);
        ((HomeActivity) mActivity).setActionBarTitle(mTripTitle);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        ((HomeActivity) mActivity).handleFrames(false);
        super.onDestroy();
    }
}
