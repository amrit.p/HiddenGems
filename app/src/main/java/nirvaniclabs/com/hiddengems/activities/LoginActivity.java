package nirvaniclabs.com.hiddengems.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.TextView;

import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.fragments.LoginFragment;
import nirvaniclabs.com.hiddengems.fragments.SignupFragment;
import nirvaniclabs.com.hiddengems.utils.HiddenGemsApplication;
import nirvaniclabs.com.hiddengems.utils.Utility;


/**
 * This activity is used to handle login and signup of a user.
 * It is parent for all the fragments used for login process.
 * {@link LoginFragment}
 * {@link SignupFragment}
 */
public class LoginActivity extends AppCompatActivity {

    private HiddenGemsApplication mHiddenGemsApplication;

    private FragmentManager mFragmentManager;
    private Toolbar mToolbar;
    private TextView mTextViewActionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (Utility.isLargeScreen(this))
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            else
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            mHiddenGemsApplication = (HiddenGemsApplication) getApplication();

            setContentView(R.layout.activity_login);
            initViews();

            setSupportActionBar(mToolbar);

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
            }

            mFragmentManager = getSupportFragmentManager();

            replaceFragment(new LoginFragment(), "", LoginFragment.TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void replaceFragment(Fragment fragment, String actionBarTitle, String tag) {

        if (mFragmentManager == null)
            return;

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        if (!tag.equals(LoginFragment.TAG)) {
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        }
        fragmentTransaction.replace(R.id.frame_content, fragment, tag);
        fragmentTransaction.commit();

        setActionBarTitle(actionBarTitle);
    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbarlayout);
        mTextViewActionBarTitle = (TextView) findViewById(R.id.text_toolbarTitle);
    }

    public void setActionBarTitle(String actionBarTitle) {
        if (!TextUtils.isEmpty(actionBarTitle))
            mTextViewActionBarTitle.setText(actionBarTitle);
        else
            mTextViewActionBarTitle.setText("");
    }

    @Override
    protected void onDestroy() {
        Utility.makeInstanceNull();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (mFragmentManager.getBackStackEntryCount() > 0) {
                    mFragmentManager.popBackStack();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            mFragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
