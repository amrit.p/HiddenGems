package nirvaniclabs.com.hiddengems.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.pkmmte.view.CircularImageView;

import de.greenrobot.event.EventBus;
import nirvaniclabs.com.hiddengems.R;
import nirvaniclabs.com.hiddengems.broadcastreceiversandservices.PauseTripEvent;
import nirvaniclabs.com.hiddengems.broadcastreceiversandservices.TripProgressEvents;
import nirvaniclabs.com.hiddengems.customwidgets.CustomButton;
import nirvaniclabs.com.hiddengems.customwidgets.CustomTextView;
import nirvaniclabs.com.hiddengems.fragments.CategoryFragment;
import nirvaniclabs.com.hiddengems.fragments.TripFragment;
import nirvaniclabs.com.hiddengems.utils.Constants;
import nirvaniclabs.com.hiddengems.utils.HiddenGemsApplication;
import nirvaniclabs.com.hiddengems.utils.Utility;

/**
 * This activity is parent activity of all the fragments needed for HiddenGems application.
 * It also contains navigation items contained in drawer menu.
 */
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private HiddenGemsApplication mHiddenGemsApplication;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private TextView mTextViewActionBarTitle;
    private FragmentManager mFragmentManager;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    public static LatLng mLatLngSource, mLatLngDestination;
    private NavigationView mNavigationView;
    private final Handler mDrawerActionHandler = new Handler();
    private RelativeLayout mRelativeLayoutTripProgress;
    private ProgressBar mTripProgressBar;
    private CustomTextView mUserName, mTextViewTripProgress, mTextViewTripName;
    private CircularImageView mProfileImage;
    private CustomButton mButtonPauseTrip;
    private boolean mIsTripCompleted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            if (Utility.isLargeScreen(this))
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            else
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            mHiddenGemsApplication = (HiddenGemsApplication) getApplication();

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            initViews();

            //
            setUpActionBar();

            mActionBarDrawerToggle = new ActionBarDrawerToggle(HomeActivity.this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer) {

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }
            };

            mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

            mActionBarDrawerToggle.syncState();

            mActionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            mFragmentManager = getSupportFragmentManager();

            setUpNavigationView();

            replaceFragment(new CategoryFragment(), getString(R.string.app_name), CategoryFragment.TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @param showFilterFrame if true displays filter screen
     */
    public void handleFrames(boolean showFilterFrame) {

        if (showFilterFrame) {
            findViewById(R.id.filter_frame_content).setVisibility(View.VISIBLE);
            findViewById(R.id.frame_content).setVisibility(View.GONE);
        } else {
            findViewById(R.id.frame_content).setVisibility(View.VISIBLE);
            findViewById(R.id.filter_frame_content).setVisibility(View.GONE);

            Fragment topFragment = getTopFragment(mFragmentManager);
            if ((topFragment) != null && topFragment instanceof TripFragment) {
                topFragment.setMenuVisibility(true);
            }
        }
    }

    /**
     * This method set up navigation view and adds navigation item
     * selected listener to navigation items.
     */
    private void setUpNavigationView() {
        View headerView = mNavigationView.getHeaderView(0);
        mNavigationView.setNavigationItemSelectedListener(this);
        mProfileImage = (CircularImageView) headerView.findViewById(R.id.image_profile);
        mUserName = (CustomTextView) headerView.findViewById(R.id.text_user_name);
        mRelativeLayoutTripProgress = (RelativeLayout) headerView.findViewById(R.id.relative_lay_trip_progress_container);
        mTripProgressBar = (ProgressBar) headerView.findViewById(R.id.progress_bar_trip);
        mTextViewTripProgress = (CustomTextView) headerView.findViewById(R.id.text_trip_progress);
        mTextViewTripName = (CustomTextView) headerView.findViewById(R.id.text_trip_name);
        mButtonPauseTrip = (CustomButton) headerView.findViewById(R.id.button_pasue_trip);

        mUserName.setText("Chaitanya Kulkarni");
        mProfileImage.setImageResource(R.drawable.ic_cast_disabled_light);
        mRelativeLayoutTripProgress.setVisibility(View.GONE);
        mTripProgressBar.setMax(1000);
    }

    /**
     * This method initializes action bar and it's title
     */
    private void setUpActionBar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mTextViewActionBarTitle.setText(getString(R.string.app_name));
        mTextViewActionBarTitle.setGravity(Gravity.CENTER);
    }

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbarlayout);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mTextViewActionBarTitle = (TextView) findViewById(R.id.text_toolbarTitle);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        }

        Fragment topFragment = getTopFragment(mFragmentManager);
        if (topFragment != null && topFragment.getTag().equals(TripFragment.TAG) && !Constants.mIsFreeRoamMode && !mIsTripCompleted) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);

            builder.setMessage("Do you wan to pause it ?")
                    .setTitle("Ongoing Trip");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    EventBus.getDefault().post(new PauseTripEvent(true));
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                    mRelativeLayoutTripProgress.setVisibility(View.GONE);

                    if (mFragmentManager.getBackStackEntryCount() > 0) {
                        mFragmentManager.popBackStack();
                    }
                }
            });


            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        } else {
            if (mFragmentManager.getBackStackEntryCount() > 0) {
                mFragmentManager.popBackStack();
            } else {
                super.onBackPressed();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addNewFragment(Fragment fragment, String actionBarTitle, String tag) {

        if (mFragmentManager == null)
            return;

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.add(R.id.filter_frame_content, fragment, tag).commit();

        setActionBarTitle(actionBarTitle);
    }

    /**
     * @param fragment       fragment to replace
     * @param actionBarTitle action bar title
     * @param tag            required to add fragment on back stack
     */
    public void replaceFragment(Fragment fragment, String actionBarTitle, String tag) {

        if (mFragmentManager == null)
            return;

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        if (!tag.equals(CategoryFragment.TAG)) {
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        }
        fragmentTransaction.replace(R.id.frame_content, fragment, tag);
        fragmentTransaction.commit();

        setActionBarTitle(actionBarTitle);
    }

    public void setActionBarTitle(String actionBarTitle) {
        if (!TextUtils.isEmpty(actionBarTitle))
            mTextViewActionBarTitle.setText(actionBarTitle);
    }

    public void setDrawerIndicatorEnabled(boolean value) {
        if (mActionBarDrawerToggle != null) {
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(value);
        }
    }

    public void setTripPogressLayoutVisibility(boolean tripPogressLayoutVisibility) {
        if (tripPogressLayoutVisibility) {
            mRelativeLayoutTripProgress.setVisibility(View.VISIBLE);
        } else
            mRelativeLayoutTripProgress.setVisibility(View.GONE);
    }

    public void enableDisableDrawer(int mode) {
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(mode);
        }
    }

    private Fragment getTopFragment(FragmentManager fragMan) {
        // Get current content Fragment
        if (fragMan.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backEntry = fragMan.getBackStackEntryAt(fragMan.getBackStackEntryCount() - 1);
            String backentryName = backEntry.getName();
            if (null != backentryName) {
                return fragMan.findFragmentByTag(backentryName);
            }
        }
        return null;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerActionHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                }, 250);
                break;
            case R.id.nav_signout:
                mDrawerLayout.closeDrawer(GravityCompat.START);

                mDrawerActionHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        finish();
                    }
                }, 250);
                break;
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        // TODO: here
//        Constants.mIsExploreMode = false;
//        Constants.mIsFreeRoamMode = false;
//        Constants.mIsNormalTrip = false;
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Utility.makeInstanceNull();
        super.onDestroy();
    }

    // This is trip pause event
    // pause the trip , send the trip details to server, save this details in local db or shared preference
    // save the visited POI details
    // save the last visited location
    // IN FREE ROAM we don't have to pause the trip
    public void onEvent(PauseTripEvent pauseTripEvent) {

        mButtonPauseTrip.setText(R.string.resume_button_text);
        // TODO: set the resume button left drawable also update the progress %

        if (mFragmentManager.getBackStackEntryCount() > 0) {
            mFragmentManager.popBackStack();
        }
    }

    // This is trip progress event.
    // this will update trip progress in drawer menu
    public void onEvent(TripProgressEvents tripProgressEvents) {
        if (TextUtils.isEmpty(tripProgressEvents.mTripName))
            return;

        mTextViewTripName.setText(tripProgressEvents.mTripName);
        mTextViewTripProgress.setText(tripProgressEvents.mTripProgress + "%");
        mTripProgressBar.setProgress((int) (tripProgressEvents.mTripProgress * 10));
        if (tripProgressEvents.mTripProgress == 100)
            mButtonPauseTrip.setText("Completed");
        else
            mButtonPauseTrip.setText(getString(R.string.pause_button_text));
        // TODO: set the pause left drawable
    }

    public void onEvent(Boolean tripCompleted) {
        mIsTripCompleted = tripCompleted;
    }

}