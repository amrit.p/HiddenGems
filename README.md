

Mentioned below is the functionality of the application :-

1. User Login and Sign-up

        User has to login / sign up for the application to access the functionality provided by the application.
    While Sign-up he will provide his name, email, password and profile image. On successful sign-up user will be redirected to login screen. 
    Once user logs in he will be redirected to the home screen of the application (Category Screen).
